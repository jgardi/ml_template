### Not everything in this template is neccessary for all projects so prune as neccesssary

### Installation
Run the following. The -e flag makes it so that changes should take affect across the environment without having to re install.
```
pip install -e .
```

### Run tests
```
nose2 -v
```

# Formatting
Use black formatting for Python.

### Logging
The _logging.py file has code for getting a logger, adding a file handler, 
and also connecting your logger to comet so that what gets logged
will show up in both standard out and comet.


### Model version management
Take advantage both git and comet.ml because they both offer different advantages.
1. save models to the models directory and make a git commit with a comment whenever you have a good model.
So we can see all the model versions along with the coresponding code and commit comments just by looking at the git repository.
2. Also upload the model to a comet.ml experiment. The advantage of comet.ml is that it is in the cloud and 
it shows graphs of learning rate, loss and final results. It also includes the git commit hash so that you know
what code that model corresponds to. 

### datasets
This scheme is inspired by [sklearn.datasets](https://scikit-learn.org/stable/modules/classes.html#module-sklearn.datasets).
Storing data in the repository makes the repository inconvenient to download. Instead we will write code to lazily download data and cache the data.
The data is cached in ~/.[project name]/data_cache. This gives 3 key advantages
1. Does not make git repository to big by putting datasets there
2. Avoids downloading data again everytime
3. Process of getting the data is automatic so you don't have to worry about tracking down the data that was
used for some experiment

### Caching
Cache in ~/.[project name]/jobcache as much as possible to avoid running the same slow code repeatedly.
