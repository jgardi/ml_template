import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="ml_template",
    version="0.0.1",
    install_requires=["pandas", "matplotlib", "comet-ml",],
)
